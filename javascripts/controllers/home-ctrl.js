app.controller('homeController', ['$scope', '$http', function($scope, $http){

   $http.get("https://hackerearth.0x10.info/api/one-push?type=json&query=list_websites").success(function(data){
      console.log(data);
      $scope.newWebsites = [];
      $scope.autoCompleteData = {};
      for (i in data.websites){
         console.log(data.websites[i].title);
         $scope.autoCompleteData[data.websites[i].title] = null;
         data.websites[i].likes = 0;
         $scope.newWebsites.push(data.websites[i]);
      }
      console.log($scope.newWebsites);
      window.localStorage.setItem('websites', JSON.stringify($scope.newWebsites));

      $("input.autocomplete").autocomplete({
         data: $scope.autoCompleteData
      });

      $scope.count = "("+data.websites.length+")";
      Materialize.toast('Website List Fetched Successfully!', 4000);
   });

   $scope.likeWebsite = function(id, url_address){
      console.log(id, url_address);
      var result = $.grep($scope.newWebsites, function(e){ return e.id == id; });
      console.log(result[0].likes);
      if($('.'+id+'-id').hasClass('clicked')){
         $('.'+id+'-id').removeClass('clicked');
         console.log('already clicked');
         result[0].likes = result[0].likes - 1;
      }
      else{
         $('.'+id+'-id').addClass('clicked');
         result[0].likes = result[0].likes + 1;
      }
      console.log(result[0].likes);
   }

   $scope.submitWebsite = function(){
      console.log('submitWebsite');
      console.log($scope.site);
      $http.post("https://hackerearth.0x10.info/api/one-push?type=json&query=push", $scope.site)
      .success(function(response){
         console.log(response);
         Materialize.toast(response.message, 4000);
      })
      .error(function(response){
         console.log(response);
         Materialize.toast(response, 4000);
      });
      $scope.site={};
      $scope.addwebsite.$setPristine();
      $scope.addwebsite.$setUntouched();
   }
}]);
