app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
   $stateProvider
      .state('home', {
         url: '/home',
         templateUrl: 'templates/main.html',
         controller: 'homeController'
      });
   $urlRouterProvider.otherwise('home');
}]);
