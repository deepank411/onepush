# One Push
One Push is a tool to find the best online website.

### Installation
One Push requires Node.js, NPM, Gulp and Bower to run.

- Unzip the source code.
- Install the dependencies and devDependencies and start the server using gulp.
- The commands to run are:
```sh
$ cd onepush
$ npm install (sudo maybe required)
$ bower install
$ gulp
```
- Browser window will open automatically on port 3000.
